FROM registry.suse.com/bci/python
MAINTAINER Brice DEKANY
ADD . /work/
RUN pip3 install -r /work/requirements.txt
EXPOSE 9000
CMD ["python3", "/work/app/app.py"]
